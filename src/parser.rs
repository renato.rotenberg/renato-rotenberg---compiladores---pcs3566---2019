use crate::statement::{StatementType, Statement};
use crate::predicates::{is_varname, is_relational, is_operation, is_number, is_num_or_var};

pub fn parser<'a>(tokens_vec: Vec<Vec<&'a str>>) -> Vec<Statement<'a>> {
    let mut parsed_statements = Vec::new();

    // Processar todos os tokens de cada linha
    for tokens in tokens_vec {
        let mut stmt = Statement {
            line_number: "0",
            stmt: StatementType::Rem,
        };
        let mut iter = tokens.iter().peekable();

        // Get line number
        if let Some(&num_str) = iter.next() {
            if !is_number(num_str) {
                panic!("Erro no programa, número da linha errada: {}", num_str);
            }
            stmt.line_number = num_str;
        } else {
            panic!("Linha vazia encontrada");
        }

        // Parse statement
        if let Some(&stmt_str) = iter.next() {
            match stmt_str {
                "LET" => {
                    // Get variable
                    let var_name = *iter.next().unwrap();
                    if !is_varname(var_name) {
                        panic!("nome de variavel {} inválido", var_name);
                    }
                    // Check equal sign
                    let equal_sign = *iter.next().unwrap();
                    if equal_sign != "=" {
                        panic!(
                            "Linha {}: esperado \"=\", encontrado {}",
                            stmt.line_number, equal_sign
                        );
                    }
                    // Collect expression
                    let expr = parse_expression(stmt.line_number, &mut iter);
                    stmt.stmt = StatementType::Let { var_name, expr };
                }

                "READ" => {
                    let mut vars = Vec::new();
                    while let Some(&var) = iter.next() {
                        if !is_varname(var) {
                            panic!("nome de variavel {} inválido", var);
                        }
                        vars.push(var);
                        // Check comma
                        if let Some(&comma) = iter.next() {
                            if comma != "," {
                                panic!(
                                    "Linha {}: esperado \",\", encontrado {}",
                                    stmt.line_number, comma
                                );
                            }
                        }
                    }
                    stmt.stmt = StatementType::Read(vars);
                }

                "DATA" => {
                    let mut data = Vec::new();
                    while let Some(&num) = iter.next() {
                        if !is_number(num) {
                            panic!("número {} inválido", num);
                        }
                        data.push(num);
                        // Check comma
                        if let Some(&comma) = iter.next() {
                            if comma != "," {
                                panic!(
                                    "Linha {}: esperado \",\", encontrado {}",
                                    stmt.line_number, comma
                                );
                            }
                        }
                    }
                    stmt.stmt = StatementType::Data(data);
                }

                "INPUT" => {
                    let var = *iter.next().unwrap();
                    if !is_varname(var) {
                        panic!("nome de variavel {} inválido", var);
                    }
                    stmt.stmt = StatementType::Input(var);
                }

                "PRINT" => {
                    let mut vars = Vec::new();
                    while let Some(&var) = iter.next() {
                        if !is_varname(var) {
                            panic!("nome de variavel {} inválido", var);
                        }
                        vars.push(var);
                        // Check comma
                        if let Some(&comma) = iter.next() {
                            if comma != "," {
                                panic!(
                                    "Linha {}: esperado \",\", encontrado {}",
                                    stmt.line_number, comma
                                );
                            }
                        }
                    }
                    stmt.stmt = StatementType::Print(vars);
                }

                "GOTO" => {
                    let num = *iter.next().unwrap();
                    if !is_number(num) {
                        panic!("Número {} inválido", num);
                    }
                    stmt.stmt = StatementType::Goto(num);
                }

                "IF" => {
                    // Collect expression 1
                    let expr1 = parse_expression(stmt.line_number, &mut iter);
                    // Get Relational
                    let relational = *iter.next().unwrap();
                    if !is_relational(relational) {
                        panic!(
                            "Linha {}: esperado relacional, encontrado {}",
                            stmt.line_number, relational
                        );
                    }
                    // Collect expression 2
                    let expr2 = parse_expression(stmt.line_number, &mut iter);
                    // Check "THEN"
                    let then = *iter.next().unwrap();
                    if then != "THEN" {
                        panic!(
                            "Linha {}: \"THEN\" esperado, encontrado {}",
                            stmt.line_number, then
                        );
                    }
                    // Get line number
                    let line_number = *iter.next().unwrap();
                    if !is_number(line_number) {
                        panic!("Erro no programa, número da linha errada: {}", line_number);
                    }
                    stmt.stmt = StatementType::If {
                        expr1,
                        relational,
                        expr2,
                        line_number,
                    };
                }

                "FOR" => {
                    // Get variable
                    let var_name = *iter.next().unwrap();
                    if !is_varname(var_name) {
                        panic!("nome de variavel {} inválido", var_name);
                    }
                    // Check equal sign
                    let equal_sign = *iter.next().unwrap();
                    if equal_sign != "=" {
                        panic!(
                            "Linha {}: esperado \"=\", encontrado {}",
                            stmt.line_number, equal_sign
                        );
                    }
                    // Collect expression 1
                    let expr1 = parse_expression(stmt.line_number, &mut iter);
                    // Check "TO"
                    let to = *iter.next().unwrap();
                    if to != "TO" {
                        panic!(
                            "Linha {}: esperado \"TO\", encontrado {}",
                            stmt.line_number, to
                        );
                    }
                    // Collect expression 2
                    let expr2 = parse_expression(stmt.line_number, &mut iter);
                    stmt.stmt = StatementType::For {
                        var_name,
                        expr1,
                        expr2,
                    };
                }

                "NEXT" => {
                    // Get variable
                    let var_name = *iter.next().unwrap();
                    if !is_varname(var_name) {
                        panic!("nome de variavel {} inválido", var_name);
                    }
                    stmt.stmt = StatementType::Next(var_name);
                }

                "END" => stmt.stmt = StatementType::End,

                "STOP" => {}

                "DEF" => {}

                "GOSUB" => {}

                "RETURN" => {}

                "DIM" => {}

                "REM" => {}

                _ => panic!("Comando desconhecido {}", stmt_str),
            }
        } else {
            panic!("Linha {} sem comando encontrada", stmt.line_number);
        }

        parsed_statements.push(stmt);
    }

    parsed_statements
}

fn parse_expression<'a>(
    line_number: &str,
    iter: &mut std::iter::Peekable<std::slice::Iter<'_, &'a str>>,
) -> Vec<&'a str> {
    let mut expr = Vec::new();

    // Pegar primeira constante ou variável
    let num_or_var = *iter.next().unwrap();
    if !is_num_or_var(num_or_var) {
        panic!(
            "Linha {}: esperado número ou variável, encontrado {}",
            line_number, num_or_var
        );
    }
    expr.push(num_or_var);

    // Conferir se próximo token é um operador, para continuar a processar expressão
    if let Some(&&operation) = iter.peek() {
        if !is_operation(operation) {
            return expr;
        }
    }
    while let Some(&operation) = iter.next() {
        // Pegar operação
        if !is_operation(operation) {
            panic!(
                "Linha {}: esperado operação, encontrado {}",
                line_number, operation
            );
        }
        expr.push(operation);
        // Pegar próxima constante ou variável
        let num_or_var = *iter.next().unwrap();
        if !is_num_or_var(num_or_var) {
            panic!(
                "Linha {}: esperado número ou variável, encontrado {}",
                line_number, num_or_var
            );
        }
        expr.push(num_or_var);
        // Conferir se próximo token é um operador, para continuar a processar expressão
        if let Some(&&operation) = iter.peek() {
            if !is_operation(operation) {
                break;
            }
        }
    }

    expr
}
