use std::collections::HashSet;
use std::fs;

use crate::statement::{StatementType, Statement};
use crate::predicates::is_number;

const LEFT_PAD: &str = "          ";
const LINE_PREFIX: &str = "L_";
const VAR_PREFIX: &str = "VAR_";
const CONST_PREFIX: &str = "CONST_";
const IF_TEMP_PREFIX: &str = "IF_TEMP_";
const NE_FALSE_PREFIX: &str = "NE_FALSE_";
const FOR_TEST_PREFIX: &str = "FOR_TEST_";
const FOR_END_PREFIX: &str = "FOR_END_";
const DATA_PREFIX: &str = "DATA_";

pub fn ir_to_asm(ir: &[Statement]) -> Vec<String> {
    let mut program = Vec::new();
    let mut var_set  = HashSet::new();
    let mut const_set = HashSet::new();
    let mut rel_helper_set= HashSet::new();
    let mut data_vec = Vec::new();
    let mut for_stack = Vec::new();
    let mut read_count = 0;
    let mut if_io_lib = false;

    // Carregamento do programa hardcoded
    program.push(format!("{} @   /6000", LEFT_PAD));

    for stmt in ir {
        // Imprimir label baseada no número da linha
        program.push(format!("{}{}", LINE_PREFIX, stmt.line_number));
        // Lidar com statement
        match &stmt.stmt {
            StatementType::Let {
                var_name,
                expr
            } => {
                var_set.insert(var_name.to_string());
                program.append(&mut compile_expr(stmt.line_number, expr, &var_set, &mut const_set));
                program.push(format!("{} MM  {}{}", LEFT_PAD, VAR_PREFIX, var_name));
            },

            StatementType::Read(vars) => {
                for &var in vars {
                    var_set.insert(var.to_string());
                    program.push(format!("{} LD  {}{}", LEFT_PAD, DATA_PREFIX, read_count));
                    program.push(format!("{} MM  {}{}", LEFT_PAD, VAR_PREFIX, var));
                    read_count += 1;
                }
            },

            StatementType::Data(data) => {
                for &num in data {
                    data_vec.push(num);
                }
            },

            StatementType::Input(var) => {
                if_io_lib = true;
                var_set.insert(var.to_string());
                program.push(format!("{} SC  GETARG", LEFT_PAD));
                program.push(format!("{} MM  {}{}", LEFT_PAD, VAR_PREFIX, var));
            }

            StatementType::Print(vars) => {
                if_io_lib = true;
                for &var in vars {
                    if !var_set.contains(var) {
                        panic!("Linha {}: Variável {} utilizada antes de ser definida", stmt.line_number, var);
                    }
                    program.push(format!("{} LD  {}{}", LEFT_PAD, VAR_PREFIX, var));
                    program.push(format!("{} SC  PRTBYTE", LEFT_PAD));
                }
            },

            StatementType::Goto(line_number) => {
                program.push(format!("{} JP {}{}", LEFT_PAD, LINE_PREFIX, line_number));
            },

            StatementType::If {
                expr1,
                relational,
                expr2,
                line_number
            } => {
                rel_helper_set.insert(stmt.line_number.to_string());
                match *relational {
                    "=" => {
                        program.append(&mut compile_expr(stmt.line_number, expr1, &var_set, &mut const_set));
                        program.push(format!("{} MM  {}{}", LEFT_PAD, IF_TEMP_PREFIX, stmt.line_number));
                        program.append(&mut compile_expr(stmt.line_number, expr2, &var_set, &mut const_set));
                        program.push(format!("{} -   {}{}", LEFT_PAD, IF_TEMP_PREFIX, stmt.line_number));
                        program.push(format!("{} JZ  {}{}", LEFT_PAD, LINE_PREFIX, line_number));
                    },

                    ">" => {
                        program.append(&mut compile_expr(stmt.line_number, expr1, &var_set, &mut const_set));
                        program.push(format!("{} MM  {}{}", LEFT_PAD, IF_TEMP_PREFIX, stmt.line_number));
                        program.append(&mut compile_expr(stmt.line_number, expr2, &var_set, &mut const_set));
                        program.push(format!("{} -   {}{}", LEFT_PAD, IF_TEMP_PREFIX, stmt.line_number));
                        program.push(format!("{} JN  {}{}", LEFT_PAD, LINE_PREFIX, line_number));
                    },

                    ">=" => {
                        program.append(&mut compile_expr(stmt.line_number, expr1, &var_set, &mut const_set));
                        program.push(format!("{} MM  {}{}", LEFT_PAD, IF_TEMP_PREFIX, stmt.line_number));
                        program.append(&mut compile_expr(stmt.line_number, expr2, &var_set, &mut const_set));
                        program.push(format!("{} -   {}{}", LEFT_PAD, IF_TEMP_PREFIX, stmt.line_number));
                        program.push(format!("{} JN  {}{}", LEFT_PAD, LINE_PREFIX, line_number));
                        program.push(format!("{} JZ  {}{}", LEFT_PAD, LINE_PREFIX, line_number));
                    },

                    "<" => {
                        program.append(&mut compile_expr(stmt.line_number, expr2, &var_set, &mut const_set));
                        program.push(format!("{} MM  {}{}", LEFT_PAD, IF_TEMP_PREFIX, stmt.line_number));
                        program.append(&mut compile_expr(stmt.line_number, expr1, &var_set, &mut const_set));
                        program.push(format!("{} -   {}{}", LEFT_PAD, IF_TEMP_PREFIX, stmt.line_number));
                        program.push(format!("{} JN  {}{}", LEFT_PAD, LINE_PREFIX, line_number));
                    },

                    "<=" => {
                        program.append(&mut compile_expr(stmt.line_number, expr2, &var_set, &mut const_set));
                        program.push(format!("{} MM  {}{}", LEFT_PAD, IF_TEMP_PREFIX, stmt.line_number));
                        program.append(&mut compile_expr(stmt.line_number, expr1, &var_set, &mut const_set));
                        program.push(format!("{} -   {}{}", LEFT_PAD, IF_TEMP_PREFIX, stmt.line_number));
                        program.push(format!("{} JN  {}{}", LEFT_PAD, LINE_PREFIX, line_number));
                        program.push(format!("{} JZ  {}{}", LEFT_PAD, LINE_PREFIX, line_number));
                    },

                    "<>" => {
                        program.append(&mut compile_expr(stmt.line_number, expr1, &var_set, &mut const_set));
                        program.push(format!("{} MM  {}{}", LEFT_PAD, IF_TEMP_PREFIX, stmt.line_number));
                        program.append(&mut compile_expr(stmt.line_number, expr2, &var_set, &mut const_set));
                        program.push(format!("{} -   {}{}", LEFT_PAD, IF_TEMP_PREFIX, stmt.line_number));
                        program.push(format!("{} JZ  {}{}", LEFT_PAD, NE_FALSE_PREFIX, stmt.line_number));
                        program.push(format!("{} JP  {}{}", LEFT_PAD, LINE_PREFIX, line_number));
                        program.push(format!("{}{}", NE_FALSE_PREFIX, stmt.line_number));
                    }

                    _ => {}
                }
            },

            StatementType::For {
                var_name,
                expr1,
                expr2,
            } => {
                var_set.insert(var_name.to_string());
                program.append(&mut compile_expr(stmt.line_number, expr1, &var_set, &mut const_set));
                program.push(format!("{} MM  {}{}", LEFT_PAD, VAR_PREFIX, var_name));
                program.push(format!("{}{}", FOR_TEST_PREFIX, stmt.line_number));
                program.append(&mut compile_expr(stmt.line_number, expr2, &var_set, &mut const_set));
                program.push(format!("{} -   {}{}", LEFT_PAD, VAR_PREFIX, var_name));
                program.push(format!("{} JZ  {}{}", LEFT_PAD, FOR_END_PREFIX, stmt.line_number));
                for_stack.push(stmt.line_number);
            },

            StatementType::Next(var_name) => {
                let for_line = for_stack.pop().unwrap();
                const_set.insert("1".to_string());
                program.push(format!("{} LD  {}{}", LEFT_PAD, VAR_PREFIX, var_name));
                program.push(format!("{} +   {}{}", LEFT_PAD, CONST_PREFIX, "1"));
                program.push(format!("{} MM  {}{}", LEFT_PAD, VAR_PREFIX, var_name));
                program.push(format!("{} JP  {}{}", LEFT_PAD, FOR_TEST_PREFIX, for_line));
                program.push(format!("{}{}", FOR_END_PREFIX, for_line));
            },

            StatementType::End => program.push(format!("{} CN  0", LEFT_PAD)),

            StatementType::Stop => {}

            StatementType::Def => {}

            StatementType::GoSub => {}

            StatementType::Return => {}

            StatementType::Dim => {}

            StatementType::Rem => {
                // Ignore Rem
            },

        }
    }

    // Put Vars, Const, Data and Helper at the end
    for var in var_set {
        program.push(format!("{}{}", VAR_PREFIX, var));
        program.push(format!("{} K   0", LEFT_PAD));
    }
    for const_var in const_set {
        program.push(format!("{}{}", CONST_PREFIX, const_var));
        program.push(format!("{} K   {}", LEFT_PAD, const_var));
    }
    for (i, data) in data_vec.iter().enumerate() {
        program.push(format!("{}{}", DATA_PREFIX, i));
        program.push(format!("{} K   {}", LEFT_PAD, data));
    }
    for rel_helper in rel_helper_set {
        program.push(format!("{}{}", IF_TEMP_PREFIX, rel_helper));
        program.push(format!("{} K   0", LEFT_PAD));
    }

    // Put Input/Output library if necessary
    if if_io_lib {
        let io_lib = fs::read_to_string("asmlib/io.asm").unwrap();
        program.push("".to_string());
        for line in io_lib.lines() {
            program.push(line.to_string());
        }
        program.push("".to_string());
    }

    program.push(format!("{} # {}", LEFT_PAD, program[1]));

    program
}

fn compile_expr(line_number: &str, expr: &[&str], var_set: &HashSet<String>, const_set: &mut HashSet<String>) -> Vec<String> {
    let mut result = Vec::new();

    let mut iter = expr.iter();

    let num_or_var = *iter.next().unwrap();

    if is_number(num_or_var) {
        const_set.insert(num_or_var.to_string());
        result.push(format!("{} LD  {}{}", LEFT_PAD, CONST_PREFIX, num_or_var));
    }
    else /* is_varname */{
        if !var_set.contains(num_or_var) {
            panic!("Linha {}: Variável {} utilizada antes de ser definida", line_number, num_or_var);
        }
        result.push(format!("{} LD  {}{}", LEFT_PAD, VAR_PREFIX, num_or_var));
    }

    while let Some(&operation) = iter.next() {
        let num_or_var = *iter.next().unwrap();
        if is_number(num_or_var) {
            const_set.insert(num_or_var.to_string());
            result.push(format!("{} {}   {}{}", LEFT_PAD, operation, CONST_PREFIX, num_or_var));
        }
        else /* is_varname */ {
            if !var_set.contains(num_or_var) {
                panic!("Linha {}: Variável {} utilizada antes de ser definida", line_number, num_or_var);
            }
            result.push(format!("{} {}   {}{}", LEFT_PAD, operation, VAR_PREFIX, num_or_var));
        }
    }

    result
}
