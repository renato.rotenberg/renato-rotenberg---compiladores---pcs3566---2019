use regex::Regex;

pub fn tokenizer(text: &str) -> Vec<Vec<&str>> {
    // Expressão regular que define os tokens possíveis da linguagem BASIC
    let re = Regex::new(r#"\d*\.?\d+(?:E-?\d+)?|SIN|COS|TAN|ATN|EXP|ABS|LOG|SQR|RND|INT|FN[A-Z]|LET|READ|DATA|PRINT|INPUT|GOTO|IF|FOR|NEXT|END|DEF|GOSUB|RETURN|DIM|REM|TO|THEN|STEP|STOP|[A-Z]\d?|".*?"|<>|>=|<=|\S"#).unwrap();
    let mut tokens = Vec::new();
    for line in text.lines() {
        tokens.push(re.find_iter(line).map(|token| token.as_str()).collect())
    }
    tokens
}
