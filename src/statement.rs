// Variante que guarda os detalhes de cada tipo de operação
#[derive(Debug)]
pub enum StatementType<'a> {
    Let {
        var_name: &'a str,
        expr: Vec<&'a str>
    },
    Read(Vec<&'a str>),
    Data(Vec<&'a str>),
    Input(&'a str),
    Print(Vec<&'a str>),
    Goto(&'a str),
    If {
        expr1: Vec<&'a str>,
        relational: &'a str,
        expr2: Vec<&'a str>,
        line_number: &'a str
    },
    For {
        var_name: &'a str,
        expr1: Vec<&'a str>,
        expr2: Vec<&'a str>,
    },
    Next(&'a str),
    End,
    Stop,
    Def,
    GoSub,
    Return,
    Dim,
    Rem
}

// Struct que define uma operação
#[derive(Debug)]
pub struct Statement<'a> {
    pub line_number: &'a str,
    pub stmt: StatementType<'a>
}
