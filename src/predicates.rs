pub fn is_varname (string: &str) -> bool {
    (string.len() == 1 || string.len() == 2 ) && string.chars().nth(0).unwrap().is_alphabetic()
}

pub fn is_relational (string: &str) -> bool {
    match string {
        "="|"<"|"<="|">"|">="|"<>" => true,
        _ => false
    }
}

pub fn is_operation (string: &str) -> bool {
    match string {
        "+"|"-"|"*"|"/"|"^" => true,
        _ => false
    }
}

pub fn is_number (string: &str) -> bool {
    string.parse::<i32>().is_ok()
}

pub fn is_num_or_var (string: &str) -> bool {
    is_number(string) || is_varname(string)
}
