mod statement;
mod tokenizer;
mod predicates;
mod parser;
mod ir_to_asm;

use std::fs;
use std::env;

use tokenizer::tokenizer;
use parser::parser;
use ir_to_asm::ir_to_asm;

// Ponto de entrada do compilador
fn main() {
    // Abrir programa a ser compilado
    let args = env::args().collect::<Vec<_>>();
    if args.len() < 2 {
        panic!("Por favor forneça o arquivo BASIC como argumento de execução");
    }
    let basic_file = &args[1];

    // Ler programa BASIC
    let text = fs::read_to_string(basic_file).unwrap();

    // Gerar tokens
    let tokens = tokenizer(&text);

    println!("{:#?}", tokens);

    // Passar tokens pelo parser para gerar a representação intemediária
    let parsed_statements= parser(tokens);

    println!("{:#?}", parsed_statements);

    // Gerar assembly
    let program = ir_to_asm(&parsed_statements);

    println!("{:#?}", program);

    // Trasnformar programa em uma única string
    let program = program.join("\n");

    // Gerar filename do arquivo
    let asm_file = {
        if let Some(i) = basic_file.find('.') {
            let name: String = basic_file.chars().take(i).collect();
            format!("{}.asm", name)
        } else {
            format!("{}.asm", basic_file)
        }
    };

    // Salvar assembly gerado
    fs::write(asm_file, program).expect("Unable to write file");
}
